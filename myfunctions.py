import json
import zerorpc

class RPCFunctions(object):
    def __init__(self,PluginManager):
        self.value = [None]
        self.return_value = []
        self.PluginManager = PluginManager

    def hello(self, name):
        return "Hello "+name+"!"

    def hash(self, hashof):
        client = zerorpc.Client()
        client.connect('tcp://127.0.1.1:5001')
        return client.do(hashof)

    def pdf(self, pdfpath):
        client = zerorpc.Client()
        client.connect('tcp://127.0.1.1:5002')
        return client.do(pdfpath)

    def unloadPlugins(self,plugins):
        return self.PluginManager.unloadPlugin(plugins)
    

    def commandParser(self, json_data):
        parsed_json = json.dumps(json_data)
        parsed_json = json.loads(parsed_json)
        command = parsed_json['command']

        if command == u'hash':
            self.value.append(self.hash(parsed_json['value']))
        if command == u'hello':
            self.value.append(self.hello(parsed_json['value']))
        if command == u'unloadPlugin':
            self.value.append(self.unloadPlugins(parsed_json['value']))

        self.return_value = self.value
        self.value = [None]
        return self.return_value
    


    """
    def unloadPlugins(self, plugins):
        for plugin in plugins:
            if plugin+'.aci' in self.loadedPlugins:
                status = subprocess.Popen(['sudo','rkt','--debug','--insecure-options=image','run',PLUGINDIRECTORY+plugin['plugindir']+'/'+plugin['pluginname'],'--port=rpc:5001','--volume','public,kind=host,source='+PLUGINPUBLICDIR])

                if status.wait() == 1:
                    failed.append(plugin['pluginname'])
                else:
                    successfull.append(plugin['pluginname'])
                    self.runningServices.append(plugin['pluginname'])
    return failed, successfull
    """