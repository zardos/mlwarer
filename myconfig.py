#socket-settings
HOST = '127.0.1.1'
PORT = 31337

#working- + plugin-directory-settings
PATH = '/home/muchacho/workspace/secpack/security_platform/live/'
PLUGINDIRECTORY = '/home/muchacho/workspace/secpack/security_platform/plugins/'
PLUGINPUBLICDIR = '/home/muchacho/workspace/secpack/security_platform/public/'
UUIDDIR = '/home/muchacho/workspace/secpack/security_platform/UUIDs/'


#config-path settings
CONFIG = '/home/muchacho/workspace/secpack/security_platform/myconfig.py'

#logging-settings
logFile = PATH+'logfile.log'
logFormat = '%(asctime)s [%(levelname)s] %(message)s'

#workdir
WORKDIR = PATH