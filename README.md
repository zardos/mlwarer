# mlwarer
you need rkt to be installed!
Also install the following dependecies
```
pip install subprocess zerorpc daemonocle
```
You could start the server with the following command:
```
python2 base_engine.py start
```
Stop it with
```
python2 base_engine.py stop
```
Afterwords use client.py to connect to the server, or use any RPC-Client e.g. zerorpc:
```
 zerorpc tcp://127.0.1.1:31337
```
or call a plugin directly 
```
zerorpc tcp://127.0.1.1:31337 hash bla 
```
