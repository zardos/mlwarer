#!/usr/bin/env bash

set -e

# Start the build with an empty ACI
acbuild --debug begin

# In the event of the script exiting, end the build
trap "{ export EXT=$?; acbuild --debug end && exit $EXT; }" EXIT

# Name the ACI
acbuild --debug set-name secpack.de/hashplugin

# Base on alpine
acbuild --debug dep add quay.io/aptible/alpine

# Base on alpine
acbuild --debug run -- apk update
acbuild --debug run -- apk --no-cache --update add build-base gcc abuild binutils openssl python python-dev py-pip supervisor py-psutil
acbuild --debug run -- pip install zerorpc daemonocle --no-cache-dir
acbuild --debug run -- apk del build-base gcc abuild binutils openssl python-dev py-pip fakeroot sudo libcap pax-utils openssl libattr attr tar pkgconf patch ca-certificates libssh2 curl abuild binutils-libs binutils gmp isl libgomp libatomic libgcc pkgconfig mpfr3 mpc1 gcc make musl-dev libc-dev fortify-headers g++ build-base ncurses-terminfo-base ncurses-terminfo ncurses-libs readline sqlite-libs py-setuptools musl-utils libbz2 expat libffi gdbm
acbuild --debug run -- apk --no-cache --update add libstdc++ 
acbuild --debug run -- rm -rf /var/cache/apk/*

# Add a port for rpc
acbuild --debug port add rpc tcp 5001

# Set working directory
acbuild set-working-directory /app

# Add a mount point for files to serve
acbuild --debug mount add public /app/public/

# Copy pluginfiles to container
acbuild copy /home/muchacho/workspace/secpack/security_platform/plugins/hashplugin/hash.py /app/hash.py

# Run plugin in foreground
acbuild --debug set-exec -- /usr/bin/python /app/hash.py start

# Save the ACI
acbuild --debug write --overwrite hashplugin-latest-amd64.aci

# run container in backgound-exposing port 5001
sudo systemd-run --slice=hashplugin rkt --debug  --insecure-options=image run /home/muchacho/workspace/secpack/security_platform/plugins/hashplugin/hashplugin-latest-amd64.aci --port=rpc:5001 --volume public,kind=host,source=/home/muchacho/workspace/secpack/security_platform/plugins/public
