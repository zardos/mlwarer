#!/usr/bin/python
import hashlib
import daemonocle
import zerorpc
import sys
import os
import time
import logging

HOST='0.0.0.0'
PORT=5001
#PATH = '/home/muchacho/workspace/secpack/security_platform/live/'
PATH = '/tmp/'
WORKDIR = PATH

class run(object):
    def __init__(self):
        self.h = hashlib.new('ripemd160')
    
    def do(self, foo):
        self.h.update(foo)
        return self.h.hexdigest()



class server(object):
    def __init__(self, host, port, daemon):
        self.server_address = "tcp://"+host+":"+str(port)

    def startingService(self):
        """
        3. Starting RPC-daemon
        """
        #Taking all the functions from class RPCFunctions out of myfunctions.py
        logging.info("Starting ZeroRPC-Service")
        server = zerorpc.Server(run())
        logging.info('ZeroRPC-Service started') 
        
        try:
            server.bind(self.server_address)
        except:
            print('already listening')        

        server.run()        
        logging.debug("Started process %r", process)
        logging.info("server closed")



if __name__ == '__main__':
    daemonHashEngine = daemonocle.Daemon(pidfile=PATH+'hashEngine.pid', 
                            workdir=WORKDIR, 
                            detach=False,
                            close_open_files=True,
                        )
    hashEngine = server(host=HOST,port=PORT, daemon=daemonHashEngine)
    daemonHashEngine.worker = hashEngine.startingService
    daemonHashEngine.do_action(sys.argv[1])