FROM alpine:latest
MAINTAINER muchacho (email@domain.com)

RUN apk --no-cache --update apk add build-base gcc abuild binutils openssl python python-dev py-pip py-psutil && \
    apk del .build-dependencies && \
    pip install zerorpc --no-cache-dir && pip install daemonocle --no-cache-dir && \
    apk del build-base gcc abuild binutils openssl python python-dev py-pip py-psutil && \
    rm -rf /var/cache/apk/*

ADD /home/muchacho/workspace/secpack/security_platform/plugins/hash.py /hash_service.py
ENTRYPOINT ["python", "/my_service.py start"]