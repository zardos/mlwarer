#!/usr/bin/python
import argparse
import os.path
import cPickle

import config
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import entropy
from sklearn import tree

def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        return arg
        #return open(arg, 'rb') 
        

def preprocess(filepath):
    hexed = ''
    with open(filepath, 'rb') as f:
        for chunk in iter(lambda: f.read(32), b''):
            hexed += chunk.encode('hex')            
    #adding spaces for ngram-generation
    hexed2 = ' '.join(a+b for a,b in zip(hexed[::2], hexed[1::2]))
    hexed = ' '.join(hexed[i:i+4] for i in xrange(0,len(hexed),4))
    return hexed


def tf_idf(inputfile, all_docs, tfidf_vectorizer):
    inputdocument = {inputfile}

    #<---------------calculate tf------------------>#
    print '[+++] calculating tf'
    vectorizer = CountVectorizer(min_df=0.05)    
    tf = vectorizer.fit_transform(inputdocument).A
    print "tf: ", tf
    print '[>>>] calculating tf done'    
    #tf = vectorizer.fit_transform(all_docs).A

    #<-------------calculate tfidf------------------>#
    print '[+++] calculating tf-idf'
    freq_term_matrix = tfidf_vectorizer.transform(inputdocument)
    tfidf = TfidfTransformer(norm="l2")
    tfidf.fit(freq_term_matrix)
    tf_idf_matrix = tfidf.transform(freq_term_matrix)
    print tf_idf_matrix.todense()
    
    print '[+++] calculating tf-idf-calculation done'    
    return tf, tf_idf_matrix


def getEntropy(inputfile):
    with open(inputfile, 'rb') as rawpdf:
        fileentropy = entropy.shannon_entropy(rawpdf.read())
    return fileentropy


class plugin(object):
    def __init__(self):
        print '[+] INITIALIZING PLUGIN'
        print '[++] loading all_docs'
        self.all_docs = set(cPickle.load(open(config.ALL_DOCS, 'rb')))
        print '[>>] loading all_docs.pickle done'

        print '[++] loading tfidf_vectorizer'
        self.tfidf_vectorizer = cPickle.load(open(config.TFIDF_VECTORIZER,'rb'))
        print '[>>] loading tfidf_vectorizer done'
        
        print '[++] loading regressor'
        self.regressor = cPickle.load(open(config.REGRESSOR,'rb'))
        print '[>>] loading regressor done'
        self.classifier = cPickle.load(open(config.CLASSIFIER,'rb'))
        print '[>] INITIALIZING PLUGIN DONE'

    def extractFeatures(self, preprocessed_input,inputfile):     
        print '[++] calculating tf_idf_matrix'
        tf, tf_idf_matrix = tf_idf(preprocessed_input, self.all_docs, self.tfidf_vectorizer)
        print '[>>] calculating tf_idf_matrix done'
        
        print '[++] calculating entropy'
        entropy = getEntropy(inputfile)
        print '[>>] calculating entropy done'
        #self.PDFpaths = getPDFpaths(self.plainpdf)
        #self.features = {'ngrams':self.ngrams,'entropy':self.entropy,'pdfpaths':self.PDFpaths}
        featureset = [[tf, tf_idf_matrix]]
        return featureset

    def predict(self, featureset):
        pr_regressor = self.regressor.predict(featureset)
        pr_classifier = self.classifier.predict(featureset)
        return pr_regressor,pr_classifier

    def do(self, foo):
        inputfile = foo.inputfile
        print '[+] preprocessing input'
        preprocessed_input = preprocess(inputfile)
        print '[>] preprocessing done'
        print '[+] extracting features'
        featureset = self.extractFeatures(preprocessed_input, inputfile)
        print '[>] extracting features done'
        prediction_regressor, prediction_classifier = self.predict(featureset)
        return prediction_regressor, prediction_classifier


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Gimme a pdf! nomm nomm.')
    parser.add_argument("-i", dest="inputfile", required=False,
                    help="input pdffile", metavar="FILE",
                    type=lambda x: is_valid_file(parser, x))

    args = parser.parse_args()
    plugin = plugin()

    while True:
        pdf_file = raw_input('give me a pdf-file: ')
        if not os.path.exists(pdf_file):
            return 'file not found'

        prediction_regressor, prediction_classifier = plugin.do(pdf_file)    
        print "regression-tree-rediction {} classifier-tree-prediction {}".format(prediction_regressor, prediction_classifier)