#!/usr/bin/python2
import daemonocle
import zerorpc
import sys
import os
import time
import logging
from plugin import plugin

HOST='127.0.1.1'
PORT=5002
#PATH = '/home/muchacho/workspace/secpack/security_platform/live/'
PATH = '/tmp/'
WORKDIR = PATH
PLUGINNAME = 'pdfcheck'

class run(object):
    def __init__(self):
        self.plugin = plugin()
    
    def do(self, foo):
        return self.plugin.do(foo)


class server(object):
    def __init__(self, host, port, daemon):
        self.server_address = "tcp://"+host+":"+str(port)

    def startingService(self):
        """
        3. Starting RPC-daemon
        """
        #Taking all the functions from class RPCFunctions out of myfunctions.py
        logging.info("Starting ZeroRPC-Service")
        server = zerorpc.Server(run())
        logging.info('ZeroRPC-Service started') 
        
        try:
            server.bind(self.server_address)
        except:
            print('already listening')        

        server.run()        
        logging.debug("Started process %r", process)
        logging.info("server closed")


if __name__ == '__main__':
    daemonEngine = daemonocle.Daemon(pidfile=PATH+PLUGINNAME+'Engine.pid', 
                            workdir=WORKDIR, 
                            detach=False,
                            close_open_files=True,
                        )
    engine = server(host=HOST,port=PORT, daemon=daemonEngine)
    daemonEngine.worker = engine.startingService
    daemonEngine.do_action(sys.argv[1])