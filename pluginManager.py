import os
import subprocess
import time

import myconfig
PLUGINDIRECTORY = myconfig.PLUGINDIRECTORY
PLUGINPUBLICDIR = myconfig.PLUGINPUBLICDIR
UUIDDIR = myconfig.UUIDDIR

class PluginManager(object):
    def __init__(self):
        self.nothing = None
        self.loadedPlugins = {}

    def listPlugins(self):
        plugins = []
        plugindirs = [d for d in os.listdir(PLUGINDIRECTORY)]
        for plugindir in plugindirs:
            for file in os.listdir(PLUGINDIRECTORY+plugindir):
                if file.endswith('.aci'):
                    plugins.append({'plugindir':plugindir,'pluginname':file})
        return plugins

    def loadPlugin(self,plugins):
        failed = []
        successfull = []
        
        for plugin in plugins:            
            subprocess.call(['sudo','systemd-run',
                                '--unit='+plugin['pluginname'],
                                '--slice='+plugin['pluginname'],
                                'rkt',
                                '--insecure-options=image',
                                'run',PLUGINDIRECTORY+plugin['plugindir']+'/'+plugin['pluginname'],
                                '--port=rpc:5001',
                                '--volume','public,kind=host,source='+PLUGINPUBLICDIR,
                                '--uuid-file-save='+UUIDDIR+plugin['pluginname']+'.uuid']
                            )
            time.sleep(2)
            uuidfile = open(UUIDDIR+plugin['pluginname']+'.uuid','r')
            #self.loadedPlugins[plugin['pluginname']] = uuidfile.read().splitlines()[0]
            self.loadedPlugins['plugin'+str(len(self.loadedPlugins))] = {'name':plugin['pluginname'],'uuid':uuidfile.read().splitlines()[0]}
            uuidfile.close()
            #os.remove(UUIDDIR+plugin['pluginname']+'.uuid')
            subprocess.call(['sudo','rm',UUIDDIR+plugin['pluginname']+'.uuid'])

        return self.loadedPlugins


    def unloadPlugin(self,plugins):
        for i in xrange(len(self.loadedPlugins)):
            subprocess.call(['sudo','rkt','stop',self.loadedPlugins['plugin'+str(i)]['uuid']])
            time.sleep(2)
            subprocess.call(['sudo','rkt','rm',self.loadedPlugins['plugin'+str(i)]['uuid']])
            (time).sleep(1)
            subprocess.call(['sudo','rkt','gc','--grace-period=0s'])
        return 'all plugins stopped'

        #sudo rkt stop <uuid>[:8]
        #sudo rkt rm <uuid> [:8]
        #sudo rkt gc --grace-period=0s