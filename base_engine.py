#python-deamon vs deamonize vs daemonocle

import os
import sys
import time

import logging

import daemonocle
import zerorpc
import threading

import os.path
from subprocess import call

import signal
import sys

"""
Reading settings form myconfig.py
"""
import myconfig
PATH = myconfig.PATH
CONFIG = myconfig.CONFIG
logFile = myconfig.logFile
logFormat = myconfig.logFormat
HOST = myconfig.HOST
PORT = myconfig.PORT
WORKDIR = myconfig.WORKDIR


"""
Importing my PRCFunctions
"""
from myfunctions import RPCFunctions
from pluginManager import PluginManager

PluginManager = PluginManager()

def daemon_shutdown(message, code):
    logging.info('Daemon is stopping')
    logging.debug(message)
    print 'unloading plugins'
    PluginManager.unloadPlugin('all')
    print 'unloading plugins done'
    print 'Exiting'


class BaseEngineService(object):
    def __init__(self, host, port, daemon, filename, PluginManager):
        self.server_address = "tcp://"+host+":"+str(port)
        
        #Daemon Reloader stuff
        self._filename = filename
        self._daemon = daemon
        self._file_mtime = os.stat(self._filename).st_mtime


    def startingService(self):

        logging.info("Starting Reloader-process")        
        reloaderThread = threading.Thread(name='daemon reloader', target=self.daemon_reloader)
        reloaderThread.daemon = True
        reloaderThread.start()

        """
        1. checking license
        """
        logging.info("Checking License")
        logging.info("check for base-engine updates")

        """
        2. loading plugins
        """
        logging.info("loading plugins")
        availablePlugins = PluginManager.listPlugins()
        for plugin in availablePlugins:
            print 'plugin: ',plugin['plugindir'],' found.'

        #loading all plugins
        loadedPlugins = PluginManager.loadPlugin(availablePlugins)
        print 'Successfully loaded plugins: ', loadedPlugins
        #print 'pluginloading failed for plugins: ', failed
        logging.info("check for plugin updates")

        """
        3. Starting RPC-daemon
        """
        #Taking all the functions from class RPCFunctions out of myfunctions.py
        logging.info("Starting ZeroRPC-Service")
        server = zerorpc.Server(RPCFunctions(PluginManager))
        logging.info('ZeroRPC-Service started') 
        
        try:
            server.bind(self.server_address)
        except:
            print('already listening')        

        server.run()        
        logging.debug("Started process %r", process)
        logging.info("server closed")


    #Config-File checker -> Plugin-loader?
    def file_has_changed(self):
        logging.info('Filewatcher running')
        current_mtime = os.stat(self._filename).st_mtime
        if current_mtime != self._file_mtime:
            self._file_mtime = current_mtime
            logging.info('configfile has changed! Reloadung service!')
            return True
        return False

    def daemon_reloader(self):
        while True:
            if self.file_has_changed():
                self._daemon.reload()
            time.sleep(1)
 


if __name__ == '__main__':
    import logging
    logging.basicConfig(filename=logFile,level=logging.DEBUG, format=logFormat,)

    #base engine daemon
    daemonBaseEngine = daemonocle.Daemon(pidfile=PATH+'baseEngine.pid', 
                                workdir=WORKDIR, 
                                detach=False,
                                close_open_files=True,
                                shutdown_callback=daemon_shutdown
                            )
    
    #PluginManager = PluginManager()
    baseEngine = BaseEngineService(host=HOST,port=PORT,filename=CONFIG, daemon=daemonBaseEngine, PluginManager=PluginManager)
    daemonBaseEngine.worker = baseEngine.startingService
    daemonBaseEngine.do_action(sys.argv[1])